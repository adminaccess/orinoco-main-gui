﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrinocoAccess;
using System.Net;
using System.Net.Mail;

namespace mainGUI
{
    public partial class ccContent : UserControl
    {
        OrinocoDB usersDB = new OrinocoDB("users.db");
        public OrinocoUser currentUser = null;
        NetworkCredential login;
        SmtpClient client;
        MailMessage msg;
        ConfigManager helpConfig = new ConfigManager();

        frmMainMenu parentForm;

        public ccContent()
        {      
            InitializeComponent();
        }
      
        private void ccContent_Load(object sender, EventArgs e)
        {
           
            try
            {
                usersDB.Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // Assign parent form to parentForm object
            // Allows parent content manipulation to happen
            parentForm = ((ParentForm) as frmMainMenu);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string pwHash = Hash.ToHash(txtPassword.Text);
            string[] word;
            string tmp;

            usersDB.Close();

            // Catch exeption if databse doesn't load
            // preventing program from crashing.
            try
            {
                usersDB.Load();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            

            if (txtPassword.Text == "" || txtUsername.Text == "")
            {
                MessageBox.Show("Please enter a username and a password.");
                return;
            }

            try
            {
                currentUser = usersDB.GetUserByName(txtUsername.Text);
                if (currentUser == null)
                {
                    MessageBox.Show("The user, " + txtUsername.Text + ", was not found!");
                    return;
                }
                if (currentUser.Password != pwHash)
                {
                    MessageBox.Show("Incorrect password!");
                }
                else
                {
                    // Check access level for each department

                    // Loop through the departments
                    foreach (Department dept in Enum.GetValues(typeof(Department)))
                    {
                        // Check if the current user has read access to the department
                        if (currentUser.HasDeptAccess(dept, AccessLevel.Read))
                        {
                            // Loop through the controls on the display panel
                            foreach (Control cntrl in pnlMainMenu.Controls)
                            {
                                // If the control is a button
                                // And the button has a tag
                                if (cntrl is Button && cntrl.Tag != null)
                                {
                                    // Split the tag to get the name of the dept on it's own
                                    word = cntrl.Tag.ToString().ToLower().Split(',');
                                    tmp = word[1];
                                    word = tmp.Split('.');
                                    tmp = word[0];

                                    // Compare the tag with the dept name
                                    // If the names match, enable the corresponding button
                                    if (tmp == dept.ToString().ToLower())
                                    {
                                        cntrl.Enabled = true;
                                    }
                                }
                            }
                        }
                    }

                    txtUsername.Text = null;
                    txtPassword.Text = null;
                    pnlLogin.Visible = false;
                    pnlMainMenu.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            parentForm.Update_UI();
        }

        private void deptButton_Click(object sender, EventArgs e)
        {
            Button btnCurrent = (Button)sender;
            Process deptProgram = new Process();
            string[] args;
            Department dept = Department.None;

            // Check that the dept id and exe filename have been add to the tag
            if (btnCurrent.Tag == null)
            {
                MessageBox.Show("The button, " + btnCurrent.Text + ", has not been properly configured. The tag is empty.");
                return;
            }

            // Make sure the tag had both the dept id and exe filename
            // Format for the tag is Department enum value, then a comma, then the exe filename
            // Example: 3,payroll.exe
            args = btnCurrent.Tag.ToString().Split(',');
            if (args.Count<string>() < 2)
            {
                MessageBox.Show("The button, " + btnCurrent.Text + ", has not been properly configured.");
                return;
            }

            // Check if the user has at least read acces
            dept = (Department)uint.Parse(args[0]); // convert dept id to Department
            if (currentUser.HasDeptAccess(dept, AccessLevel.Read) == false)
            {
                MessageBox.Show("You do not have access to the " + btnCurrent.Text + " department!");
                return;
            }

            // Run department program
            try
            {
                // Set the department exe filename
                deptProgram.StartInfo.FileName = args[1];

                // Set the employee ID and auth string as arguments
                deptProgram.StartInfo.Arguments = currentUser.EmployeeID + " " + currentUser.GetAuthString();

                // Run the program
                deptProgram.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(deptProgram.StartInfo.FileName + ": " + ex.Message);
            }

            parentForm.Update_UI();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (helpConfig.Exists())
            {
                helpConfig.Load();
            }
            else // Prompt the user for email details
            {
                using (var form = new EmailDetails())
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        helpConfig.Config.ReceiverEmail = form.ReceiverEmail;
                        helpConfig.Config.SenderEmail = form.SenderEmail;
                        helpConfig.Config.SenderPassword = form.SenderPassword;
                    }
                }

                helpConfig.Save();
            }
            login = new NetworkCredential(helpConfig.Config.SenderEmail, helpConfig.Config.SenderPassword);
            client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.EnableSsl = true;
            client.Credentials = login;
            msg = new MailMessage { From = new MailAddress(helpConfig.Config.SenderEmail) };
            msg.To.Add(new MailAddress(helpConfig.Config.ReceiverEmail));
            msg.Subject = txtSubject.Text;
            msg.Body = rtbMessage.Text;
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.Normal;
            msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallBack);
            string userstate = "Sending...";

            try
            {
                client.SendAsync(msg, userstate);
                //client.Send(msg);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void SendCompletedCallBack(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show(string.Format("{0} send cancelled. ", e.UserState), "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (e.Error != null)
            {
                MessageBox.Show(string.Format("{0} {1}", e.UserState), "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Your message has been successfully sent.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = false;
            pnlMainMenu.Visible = false;
            pnlHelp.Visible = true;

            parentForm.Update_UI();
        }
        
        // If the enter button is clicked
        // Attempt to login and suppress the keypress (stops the windows error sound)
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(this, new EventArgs());
                e.Handled = e.SuppressKeyPress = true;
            }
        }
    }
}