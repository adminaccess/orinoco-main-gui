﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mainGUI
{
    public partial class EmailDetails : Form
    {
        public EmailDetails()
        {
            InitializeComponent();
        }
        public string SenderEmail { get; set; }
        public string SenderPassword { get; set; }
        public string ReceiverEmail { get; set; }

        private void btnSend_Click(object sender, EventArgs e)
        {
            this.SenderEmail = txtSender.Text;
            this.SenderPassword = txtPassword.Text;
            this.ReceiverEmail = txtReceiver.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

    }

}
