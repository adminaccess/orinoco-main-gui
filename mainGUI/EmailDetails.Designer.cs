﻿namespace mainGUI
{
    partial class EmailDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtReceiver = new System.Windows.Forms.TextBox();
            this.txtSender = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblRec = new System.Windows.Forms.Label();
            this.lblSend = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtReceiver
            // 
            this.txtReceiver.Location = new System.Drawing.Point(132, 39);
            this.txtReceiver.Name = "txtReceiver";
            this.txtReceiver.Size = new System.Drawing.Size(179, 20);
            this.txtReceiver.TabIndex = 0;
            // 
            // txtSender
            // 
            this.txtSender.Location = new System.Drawing.Point(132, 65);
            this.txtSender.Name = "txtSender";
            this.txtSender.Size = new System.Drawing.Size(179, 20);
            this.txtSender.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(132, 91);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(179, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // lblRec
            // 
            this.lblRec.AutoSize = true;
            this.lblRec.Location = new System.Drawing.Point(48, 42);
            this.lblRec.Name = "lblRec";
            this.lblRec.Size = new System.Drawing.Size(78, 13);
            this.lblRec.TabIndex = 3;
            this.lblRec.Text = "Receiver Email";
            // 
            // lblSend
            // 
            this.lblSend.AutoSize = true;
            this.lblSend.Location = new System.Drawing.Point(57, 72);
            this.lblSend.Name = "lblSend";
            this.lblSend.Size = new System.Drawing.Size(69, 13);
            this.lblSend.TabIndex = 4;
            this.lblSend.Text = "Sender Email";
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(32, 98);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(90, 13);
            this.lblPass.TabIndex = 5;
            this.lblPass.Text = "Sender Password";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(236, 137);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 6;
            this.btnSend.Text = "Send Details";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // EmailDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 196);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.lblPass);
            this.Controls.Add(this.lblSend);
            this.Controls.Add(this.lblRec);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtSender);
            this.Controls.Add(this.txtReceiver);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmailDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Email Details";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtReceiver;
        private System.Windows.Forms.TextBox txtSender;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblRec;
        private System.Windows.Forms.Label lblSend;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Button btnSend;
    }
}