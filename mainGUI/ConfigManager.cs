﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace mainGUI
{
    public class HelpConfig
    {
        public string SenderEmail = "";
        public string SenderPassword = "";
        public string ReceiverEmail = "";
    }

    public class ConfigManager
    {
        private const string CONFIG_FILE = "help.cfg";
        public HelpConfig Config = new HelpConfig();

        /// <summary>
        /// Save the configuration in memory to a file.
        /// </summary>
        public void Save()
        {
            XmlSerializer writer = new XmlSerializer(typeof(HelpConfig));
            FileStream configFile = null;
            HelpConfig encryptedConfig = new HelpConfig();

            try
            {
                // Create config file
                configFile = File.Create(CONFIG_FILE);

                // Encrypt config
                encryptedConfig.SenderEmail = SecureString.Encrypt(Config.SenderEmail);
                encryptedConfig.SenderPassword = SecureString.Encrypt(Config.SenderPassword);
                encryptedConfig.ReceiverEmail = SecureString.Encrypt(Config.ReceiverEmail);

                // Write config
                writer.Serialize(configFile, encryptedConfig);
            }
            catch (Exception ex)
            {
                if (configFile != null)
                    configFile.Close();
                MessageBox.Show(ex.Message);
            }

            configFile.Close();
        }

        /// <summary>
        /// Read and load the configuration into memory.
        /// </summary>
        public void  Load()
        {
            XmlSerializer reader = new XmlSerializer(typeof(HelpConfig));
            StreamReader configFile = null;
            HelpConfig encryptedConfig;

            try
            {
                // Open config file
                configFile = new StreamReader(CONFIG_FILE);

                // Read config file
                encryptedConfig = (HelpConfig)reader.Deserialize(configFile);

                // Decrypt config
                Config.SenderEmail = SecureString.Decrypt(encryptedConfig.SenderEmail);
                Config.SenderPassword = SecureString.Decrypt(encryptedConfig.SenderPassword);
                Config.ReceiverEmail = SecureString.Decrypt(encryptedConfig.ReceiverEmail);
            }
            catch (Exception ex)
            {
                if (configFile != null)
                    configFile.Close();
                MessageBox.Show(ex.Message);
            }

            configFile.Close();
        }

        /// <summary>
        /// Check if the configuration file exists
        /// </summary>
        /// <returns>True if found, false if not found.</returns>
        public bool Exists()
        {
            return File.Exists(CONFIG_FILE);
        }
    }
}
